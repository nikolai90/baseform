import FZSelect from '@/foodzi/components/ui/fz-select/index.vue'
import FZRemoteSelect from '@/foodzi/components/ui/fz-remote-select/index.vue'
import FZInput from '@/foodzi/components/ui/fz-input/index.vue'
import FZTextArea from '@/foodzi/components/ui/fz-textarea/index.vue'
import FZSwitch from '@/foodzi/components/ui/fz-switch/index.vue'
import FZImageUpload from '@/foodzi/components/ui/fz-image-upload/index.vue'
import FZDatepicker from '@/foodzi/components/ui/fz-datepicker/index.vue';
import FZInputFromTo from '@/foodzi/components/ui/fz-input-from-to/index.vue';

export default class {
    constructor(opts) {
        this.props = opts.props

        switch (opts.type) {
            case 'string':
                this.component = FZInput
                break
            case 'select':
                this.component = FZSelect
                if (opts.props.options instanceof Function) {
                    this.props.options = opts.props.options()
                }
                break
            case 'remote-select':
                this.component = FZRemoteSelect
                break;
            case 'text':
                this.component = FZTextArea
                break
            case 'switch':
                this.component = FZSwitch
                break
            case 'datepicker':
                this.component = FZDatepicker
                break
            case 'file':
                this.component = FZImageUpload
                break
            case 'from-to':
                this.component = FZInputFromTo
                break
        }
    }
}
