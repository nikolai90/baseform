const isEmptyArray = arr => Array.isArray(arr) && arr.length === 0;
const isNullOrUndefined = value => value === null || value === undefined;

const required = value => {
    const errorMessage = 'Поле обязательно';
    if (isNullOrUndefined(value) || isEmptyArray(value) || value === false) {
        return errorMessage;
    }
    if (!String(value).trim().length) {
        return errorMessage;
    }
    return true;
}

export default {
    required
};
