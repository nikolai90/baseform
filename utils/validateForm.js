import validators from './validators'

export default (fields, model, touchedFields) => {
    const res = {}
    model.forEach(f => {
        const validations = f.validators
        //validate only touched fields
        if (!validations || !touchedFields.includes(f.fieldKey)) return

        res[f.fieldKey] = []
        validations.forEach(validationType => {
            const value = fields[f.fieldKey]
            const validationResult = validators[validationType](value)

            if (validationResult !== true) res[f.fieldKey].push(validationResult)
        })
        if (!res[f.fieldKey].length) delete res[f.fieldKey]
    })

    return res
}
